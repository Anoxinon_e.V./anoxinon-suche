const sentenceEndRegex = /\S{5,}\.(\s+)/g

function getSentenceStarts(input) {
  const output = [0]
  let match

  while ((match = sentenceEndRegex.exec(input)) !== null) {
    const index = match.index + match[0].length

    if (output[output.length - 1] < index && index < input.length) {
      output.push(index)
    }
  }

  return output
}

module.exports = { getSentenceStarts }
