function mergeRanges(rangeLists) {
  const merged = []

  // merge
  rangeLists.forEach((list) => list.forEach((item) => merged.push(item)))

  // sort
  merged.sort((a, b) => a[0] - b[0])

  // merge overlaps
  const output = []

  merged.forEach((current) => {
    if (output.length === 0) {
      output.push(current)
    } else {
      const previous = output[output.length - 1]
      const previousEnd = previous[0] + previous[1]
      const currentEnd = current[0] + current[1]

      if (previousEnd >= current[0]) {
        output[output.length - 1] = [ previous[0], Math.max(previousEnd, currentEnd) - previous[0] ]
      } else {
        output.push(current)
      }
    }
  })

  return output
}

function getRangeLists(metadata, attribute) {
  const result = []

  for (const term in metadata) {
    const tmp = metadata[term][attribute]

    if (tmp) result.push(tmp.position)
  }

  return result
}

function getHighlightRanges(metadata, attribute) {
  return mergeRanges(getRangeLists(metadata, attribute))
}

function subranges(ranges, start, end) {
  const result = []

  ranges.forEach((range) => {
    const thisStart = range[0] - start
    const thisEnd = thisStart + range[1]

    const clampedStart = Math.max(thisStart, 0)
    const clampedEnd = Math.max(0, Math.min(thisEnd, end))

    if (clampedStart !== clampedEnd && clampedStart < end - start) {
      result.push([clampedStart, clampedEnd - clampedStart])
    }
  })

  return result
}

function rangeLengthSum(ranges) {
  let result = 0

  ranges.forEach((range) => result += range[1])

  return result
}

module.exports = { getHighlightRanges, subranges, rangeLengthSum }
