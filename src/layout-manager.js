const { loadTextFile } = require('./query.js')
const { sleep } = require('./sleep.js')

function createLayoutManager(source, updateInterval) {
  function doCatch(promise) {
    promise.catch((ex) => {/* will be handled somehwere else */})
  }

  let nextResult = loadTextFile(source); doCatch(nextResult)

  ;(async () => {
    let initialAttempt = 0

    while (true) {
      try {
        console.log('trying to load ' + source)

        await nextResult

        initialAttempt = null
      } catch (ex) {
        console.warn('loading ' + source + ' failed', ex)

        if (initialAttempt !== null && initialAttempt < 5) {
          console.log('is booting, so retry faster')

          initialAttempt++

          await sleep(initialAttempt * 1000); continue
        }
      }

      await sleep(updateInterval)

      nextResult = loadTextFile(source); doCatch(nextResult)
    }
  })()

  return { query: () => nextResult }
}

module.exports = { createLayoutManager }
