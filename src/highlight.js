function addHighlighting(input, positions) {
  let output = []
  let readCursor = 0

  positions.forEach((position) => {
    const positionStart = position[0]
    const positionEnd = position[0] + position[1]

    output.push({
      text: input.substring(readCursor, positionStart),
      highlight: false
    })

    output.push({
      text: input.substring(positionStart, positionEnd),
      highlight: true
    })

    readCursor = positionEnd
  })

  output.push({
    text: input.substring(readCursor, input.length),
    highlight: false
  })

  return output
}

module.exports = { addHighlighting }
