const http = require('http')
const https = require('https')
const { readFile } = require('fs')

async function loadTextFile(url) {
  if (url.startsWith('http')) {
    url = new URL(url)

    return new Promise((resolve, reject) => {
      setTimeout(() => reject(new Error('timeout')), 1000 * 30)

      const resHandler = (res) => {
        if (res.statusCode !== 200) {
          res.resume()

          reject(new Error('invalid status code ' + res.statusCode)); return
        }

        let rawData = ''

        res.setEncoding('utf8')

        res.on('data', (chunk) => { rawData += chunk })
        res.on('end', () => {
          try {
            resolve(rawData)
          } catch (ex) {
            reject(ex)
          }
        })
      }

      if (url.protocol === 'https:') {
        https.get(url, resHandler).on('error', (err) => reject(err))
      } else if (url.protocol === 'http:') {
        http.get(url, resHandler).on('error', (err) => reject(err))
      } else {
        reject(new Error('unsupported protocol: ' + url.protocol))
      }
    })
  } else {
    return new Promise((resolve, reject) => {
      readFile(url, (err, res) => {
        if (err) reject(err)
        else resolve(res.toString('utf8'))
      })
    })
  }
}

module.exports = { loadTextFile }
