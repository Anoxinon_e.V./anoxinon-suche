const { loadTextFile } = require('./query.js')

async function loadIndexFile(url) {
  return JSON.parse(await loadTextFile(url))
}

async function loadIndexFiles(urls) {
  const contentList = await Promise.all(urls.map((url) => loadIndexFile(url)))
  const result = []

  contentList.forEach((list) => {
    if (!Array.isArray(list)) throw new Error('invalid content list')

    list.forEach((item) => result.push(item))
  })

  return result
}

module.exports = { loadIndexFiles }
