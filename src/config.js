const mediaBase = process.env.MEDIA_BASE || 'https://anoxinon.media'
const mainBase = process.env.MAIN_BASE || 'https://anoxinon.de'

const config = {
  listen: process.env.LISTEN || '9000',
  updateInterval: 15 * 60 * 1000,
  sources: [mediaBase, mainBase].map((item) => item + '/index.json'),
  frontend: [
    {
      id: 'media',
      source: mediaBase + '/suche/index.html',
      startMarker: '<section',
      endMarker: '</section>',
      formAction: '/suche/',
      template: 'media-search-widget.ejs'
    },
    {
      id: 'main',
      source: mainBase + '/suche/index.html',
      startMarker: '<section class=',
      endMarker: '</section>',
      formAction: '/suche/',
      template: 'media-search-widget.ejs'
    }
  ]
}

module.exports = { config }
