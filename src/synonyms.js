const lunr = require('lunr')

const synonymGroups = [
  ['OpenPGP', 'GPG', 'PGP']
]

function buildSynonymIndex(groups) {
  const result = {}

  groups.forEach((group) => {
    group.forEach((word1) => {
      const key = word1.toLowerCase()

      result[key] = []

      group.forEach((word2) => {
        if (word1 !== word2) {
          result[key].push(word2.toLowerCase())
        }
      })
    })
  })

  return result
}

const synonymIndex = buildSynonymIndex(synonymGroups)

const applySynonyms = (builder) => {
  const pipelineFunction = function (token) {
    const synonyms = synonymIndex[token.toString().toLowerCase()]

    if (synonyms === undefined) {
      return token
    }

    let result = [ token ]

    synonyms.forEach((synonym) => {
      result.push(
        token.clone(() => synonym)
      )
    })

    return result
  }

  lunr.Pipeline.registerFunction(pipelineFunction, 'applySynonyms')

  builder.pipeline.before(lunr.stemmer, pipelineFunction)
}

module.exports = { applySynonyms }
