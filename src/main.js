const { config } = require('./config.js')
const { createFrontend } = require('./frontend.js')
const { createIndexManager } = require('./index-manager.js')

const { app, setIndex } = createFrontend(config.frontend, config.updateInterval)

createIndexManager(config.sources, config.updateInterval, (index) => setIndex(index))

app.listen(config.listen, () => console.log('listening'))
