function randomItem(list) {
  return list[Math.round(Math.random() * (list.length - 1))]
}

module.exports = { randomItem }
