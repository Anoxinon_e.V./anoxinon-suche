const lunr = require('lunr')
require('lunr-languages/lunr.stemmer.support')(lunr)
require('lunr-languages/lunr.multi')(lunr)
require('lunr-languages/lunr.de')(lunr)
const { getHighlightRanges } = require('./ranges.js')
const { randomItem } = require('./list.js')
const { addHighlighting } = require('./highlight.js')
const { getSentenceStarts } = require('./sentences.js')
const { generateSnippet } = require('./snippet.js')
const { applySynonyms } = require('./synonyms.js')

function createIndex(content) {
  if (!Array.isArray(content)) {
    throw new Error('content is no array')
  }

  content.forEach((item) => {
    if (typeof item !== 'object') throw new Error('content item is no object')
    if (typeof item.url !== 'string') throw new Error('content item has no url')
    if (typeof item.title !== 'string') throw new Error('content item has no title')
    if (typeof item.content !== 'string') throw new Error('content item has no content')
    if (typeof item.description !== 'string') throw new Error('content item has no description')
    if (typeof item.date !== 'undefined' && typeof item.date !== 'number' && item.date !== null)
      throw new Error('content item has no date')
  })

  const contentByUrl = {}
  const originalContent = content

  content = content.map((item) => ({
    url: item.url,
    title: item.title,
    content: item.content,
    description: item.description,
    date: typeof item.date === 'number' ? item.date : null
  })).map((item) => ({
    ...item,
    sentenceStarts: getSentenceStarts(item.content)
  }))

  content.forEach((item) => contentByUrl[item.url] = item)

  const searchSuggestions = content.map((item) => item.title)

  const internalIndex = lunr(function () {
    this.use(lunr.multiLanguage('en', 'de'))
    this.use(applySynonyms)

    this.metadataWhitelist = ['position']

    this.ref('url')
    this.field('title')
    this.field('content')
    this.field('description')

    content.forEach(this.add.bind((this)))
  })

  function searchWithExtendedRanking(term) {
    let results = internalIndex.search(term)

    let newestResult = null; results.forEach((result) => {
      const item = contentByUrl[result.ref]

      if (item.date !== null && item.date > newestResult) {
        newestResult = item.date
      }
    })

    if (newestResult !== null) {
      let maxScore = 0; results.forEach((result) => maxScore = Math.max(maxScore, result.score))

      const scoreByAge = maxScore / 3
      const ageRange = 1000 * 60 * 60 * 24 * 365

      results = results.map((result) => {
        const item = contentByUrl[result.ref]

        const ageInRange = item.date !== null ? Math.min(newestResult - item.date, ageRange) : ageRange
        const scoreReducing = ageInRange * scoreByAge / ageRange

        return {
          ...result,
          score: result.score - scoreReducing
        }
      })

      results.sort((a, b) => b.score - a.score)
    }

    return results
  }

  const search = (term) => searchWithExtendedRanking(term).map((result) => {
    const item = contentByUrl[result.ref]

    const titleMatches = getHighlightRanges(result.matchData.metadata, 'title')
    const descriptionMatches = getHighlightRanges(result.matchData.metadata, 'description')
    const contentMatches = getHighlightRanges(result.matchData.metadata, 'content')

    let text
    const badDescription = item.description.length < 50
    const onlyFulltextMatches = descriptionMatches.length === 0 && contentMatches.length > 0
    const useFullText = badDescription || onlyFulltextMatches

    if (useFullText) {
      text = generateSnippet(item.content, contentMatches, item.sentenceStarts)
    } else {
      text = addHighlighting(item.description, descriptionMatches)
    }

    return {
      url: item.url,
      title: addHighlighting(item.title, titleMatches),
      text
    }
  })

  const getSuggestion = () => randomItem(searchSuggestions)

  return { search, getSuggestion, searchSuggestions }
}

module.exports = { createIndex }
