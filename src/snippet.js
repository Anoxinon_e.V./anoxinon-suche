const { subranges, rangeLengthSum } = require('./ranges.js')
const { addHighlighting } = require('./highlight.js')

const targetLength = 500

function generateSnippet(text, textMatches, sentenceStarts) {
  let result = []

  // score sentences
  const sentenceInfos = sentenceStarts.map((start, index) => {
    const end = index === sentenceStarts.length - 1 ? text.length : sentenceStarts[index + 1]
    const length = end - start
    const highlights = subranges(textMatches, start, end)
    const hasHighlights = highlights.length > 0
    const startScore = index < 3 ? 1 : 0
    const highlightScore = (hasHighlights ? 1 : 0) + Math.pow(rangeLengthSum(highlights), 2.0) * 32 / length

    const score = startScore + highlightScore

    return { start, end, length, highlights, score }
  })

  // take sentences by the score, limiting the length
  sentenceInfos.sort((a, b) => b.score - a.score)

  const minScore = sentenceInfos[0].score / 4

  const usedSentences = []
  let usedSentencesLength = 0

  for (let i = 0; i < sentenceInfos.length; i++) {
    const sentence = sentenceInfos[i]

    const tooLong = usedSentencesLength + sentence.length > targetLength

    if (tooLong) {
      if (usedSentencesLength > 0) break

      if (sentence.highlights.length > 0) {
        // start at the highlight
        const newStart = sentence.start + sentence.highlights[0][0]
        const newEnd = Math.min(newStart + targetLength, sentence.end)
        const newLength = newEnd - newStart

        usedSentences.push({
          start: newStart,
          end: newEnd,
          length: newLength,
          highlights: subranges(textMatches, newStart, newEnd),
          score: sentence.score
        }); break
      }

      usedSentences.push(sentence); break
    }

    if (sentence.score < minScore) break

    usedSentences.push(sentence)
    usedSentencesLength += sentence.length
  }

  // output the sentences
  usedSentences.sort((a, b) => a.start - b.start)

  usedSentences.forEach((sentenceInfo) => {
    const { start, end, highlights } = sentenceInfo
    const subtext = text.substring(start, end)

    addHighlighting(subtext, highlights).forEach(result.push.bind(result))
  })

  return result
}

module.exports = { generateSnippet }
