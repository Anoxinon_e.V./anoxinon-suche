const { loadIndexFiles } = require('./load-index.js')
const { createIndex } = require('./create-index.js')
const { sleep } = require('./sleep.js')

function createIndexManager(urls, updateInterval, newIndexCallback) {
  (async () => {
    let initialAttempt = 0

    while (true) {
      try {
        console.log('trying index update')

        const newData = await loadIndexFiles(urls)
        const newIndex = createIndex(newData)

        newIndexCallback(newIndex)
        initialAttempt = null

        console.log('created new index')
      } catch (ex) {
        console.warn('index update failed', ex)

        if (initialAttempt !== null && initialAttempt < 5) {
          console.log('is booting, so retry faster')

          initialAttempt++

          await sleep(initialAttempt * 1000); continue
        }
      }

      await sleep(updateInterval)
    }
  })()
}

module.exports = { createIndexManager }
