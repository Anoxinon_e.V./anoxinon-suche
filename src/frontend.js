const express = require('express')
const { resolve } = require('path')
const { createLayoutManager } = require('./layout-manager')

function createFrontend(frontends, updateInterval) {
  let index = null
  const app = express()

  const setIndex = (newIndex) => index = newIndex
  const getIndex = () => index

  app.set('view engine', 'ejs')

  frontends.forEach((frontend) => {
    app.use('/' + frontend.id + '/', createWidgetFrontend(frontend, updateInterval, getIndex))
  })

  return { app, setIndex }
}

function createWidgetFrontend(frontend, updateInterval, getIndex) {
  const router = express.Router()
  const layoutManager = createLayoutManager(frontend.source, updateInterval)

  async function renderAndWrap(res, content) {
    const layout = await layoutManager.query()
    const startIndex = layout.indexOf(frontend.startMarker)
    const endIndex = layout.indexOf(frontend.endMarker, startIndex)

    if (startIndex < 0 || endIndex < 0 || endIndex <= startIndex) {
      throw new Error()
    }

    const widgetStart = layout.substring(0, startIndex)
    const widgetEnd = layout.substring(endIndex + frontend.endMarker.length)

    res.render(frontend.template, { ...content, widgetStart, widgetEnd })
  }

  const showPage = async (req, res, next) => {
    try {
      const index = getIndex()
      const term = typeof req.body === 'object' ? req.body.q : ''

      if (typeof term !== 'string') {
        res.sendStatus(400)
        return
      }

      let errorMessage = ''
      let results = []

      if (index !== null) {
        const emptyTerm = term.trim().length === 0

        if (!emptyTerm) {
          results = index.search(term)

          if (results.length === 0) {
            errorMessage = 'Dazu haben wir leider Nichts gefunden'
          }
        }
      } else {
        errorMessage = 'Die Suche steht momentan leider nicht zur Verfügung'
      }

      await renderAndWrap(res, {
        term,
        placeholderTerm: index ? index.getSuggestion() : '',
        searchSuggestions: index ? index.searchSuggestions : [],
        results,
        errorMessage,
        formAction: frontend.formAction
      })
    } catch (ex) {
      next(ex)
    }
  }

  router.get('/', showPage)
  router.post('/', express.urlencoded({ extended: false }), showPage)
  router.get('/index.html', showPage)
  router.post('/index.html', express.urlencoded({ extended: false }), showPage)

  return router
}

module.exports = { createFrontend }
