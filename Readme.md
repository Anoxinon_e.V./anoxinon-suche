# Anoxinon-Suche

Eine Suche für Anoxinon-Inhalte auf der Basis von [Lunr](https://lunrjs.com/).

## Konfiguration

- die Konfiguration erfolgt in ``src/config.js`` und kann z.T. durch Umgebungsvariablen beeinflusst werden
- ``listen`` gibt die Portnummer oder den Unix-Socket-Pfad an, unter dem die Anwendung laufen soll
- ``updateInterval`` gibt an, wie oft die verwendeten Daten neu geladen werden sollen
- ``sources`` enthält die Pfade zu JSON-Dateien, die indexiert werden sollen
- unter ``frontend`` werden die verschiedenen Frontends (also Endpunkte zum Einbetten konfiguriert)
  - ``id`` gibt die URL an
  - ``source`` gibt an, wo die Layoutdatei liegt
  - ``startMarker`` und ``endMarker`` geben an, womit der zu ersetzende Teil der Layoutdatei anfängt und aufhört
  - ``formAction`` gibt die Ziel-URL des ausgegebenen Formulars an

Hinweise:

- Pfade können URLs oder ein Pfad im lokalen Dateisystem sein

## Benutzung

- ``npm install``
- ``npm start`` (oder ``npm run develop``, damit bei Codeänderungen automatisch ein Neustart der Anwendung erfolgt)
- Browser starten (z.B. ``http://localhost:9000/media``)

Die angezeigte Seite wird in der Form wahrscheinlich nicht schön aussehen und möglicherweise
stimmt auch die Ziel-URL des Formulars nicht. Es ist vorgesehen, die Suchseite als einzelne
Unterseite vom Webserver nicht direkt ausgeliefert wird sondern per Reverse-Proxy-Funktion an
diese Anwendung geleitet wird.

## Lizenz

anoxinon-suche - A search for content from Anoxinon  
Copyright (C) 2021 Anoxinon e.V.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
